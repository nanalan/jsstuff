import p5 from "p5";
import Turtle from "../../lib/turtle"
var sketch = function(p : p5){
	let t : Turtle;
	let black = p.color(112, 11, 38);
	let white = p.color(247, 235, 183);
	p.setup = function(){
		p.createCanvas(700, 700);
		t = new Turtle(p);
		t.penDown();
		p.colorMode(p.HSB, 100);
		p.frameRate(40);
		p.stroke(3);
		t.moveTo(400, 400);
	};
	p.draw = function(){
		let state = p.sin(p.millis() / 3000) / 2 + .5;
		let state2 = p.sin(p.millis() / 9000) / 2 + .5;
		let h = state * 23 - 9;
		if(h < 0)
			h += 100;
		let v = 60 + state * 40;
		let s = 100 - state * 20
		p.stroke(h, s, v);
		t.turnLeft(170 - 70 * state2);
		t.moveForward(140 + 190 * state);
	};
}

let myp5 = new p5(sketch)