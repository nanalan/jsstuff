import p5 from "p5";
import {genarray} from "../../lib/general";
import {dither} from "../../lib/dither";
const xc = 100;
const yc = 100;
const xd = 8;
const yd = 8;
const linemargin = 2;
const noisefreq = .2;
const noisetimefreq = .03;
const colorfreq = .4;
const colortimefreq = .0003
const colormorphproportion = 180;
var sketch = function(p : p5){
	let locs : boolean[][] = genarray(xc, () => (new Array<boolean>(yc)))

	p.setup = function(){
		p.createCanvas(xc*xd, yc*yd);
		p.colorMode(p.HSB, 100);
		p.noStroke();
	}

	let lastLoc = 0;
	p.draw = function(){
		p.background(100);
		if(p.millis() - lastLoc > 1000){
			console.log("refresh!")
			dither(dithergen, locs)
			lastLoc = p.millis();
		}
		for(let x = 0; x < xc; x++){
			for(let y = 0; y < yc; y++){
				if(locs[x][y]){
					drawHoriz(x, y, p.millis() * colortimefreq);
					drawVert(x, y,  p.millis() * colortimefreq);
				}else{
					drawVert(x, y,  p.millis() * colortimefreq);
					drawHoriz(x, y, p.millis() * colortimefreq);
				}
			}
		}
	}

	const dithergen = {
		gen : (x : number, y : number) => p.noise(x * noisefreq, y * noisefreq, p.millis() * noisetimefreq),
		w : xc,
		h : yc
	}

	function drawHoriz(x : number, y : number, offset : number){
		p.fill(p.noise(0, y * colorfreq + offset, 10000 + offset / colormorphproportion) * 100, 100, 100);
		p.rect(x * xd, y * yd + linemargin, xd, yd - linemargin * 2);
	}
	function drawVert(x : number, y : number, offset : number){
		p.fill(p.noise(10000, x * colorfreq + offset, 10000 + offset / colormorphproportion) * 100, 100, 100);
		p.rect(x * xd + linemargin, y * yd, xd - linemargin * 2, yd);
	}

}

let myp5 = new p5(sketch)