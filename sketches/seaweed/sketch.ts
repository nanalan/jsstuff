import p5 from "p5"
import Turtle from "../../lib/turtle"
var sketch = function(p : p5){
	let roots : Turtle[];
	p.setup = function(){
		p.createCanvas(800, 800)

		p.strokeWeight(3);
		
		reset();
	}
	function reset(){
		p.background(9,89,89)
		p.stroke(78,186,61)
		roots = new Array<Turtle>();
		for(let base = 10; base < 800; base += p.random(5, 200)){
			let wid = p.random(8, 18);
			for(let offset = 0; offset < wid; offset += 2){
				roots.push(
					new Turtle(p, base + offset, p.height)
				);
			}
		}
	}
	p.draw = function(){
		for(let t of roots){
			let noise = p.noise(t.x * 90, t.y *10)
			t.turnTo(
				p.map( noise, 0, 1, -140, -40)
			);
			p.strokeWeight(
				p.map( Math.abs(noise - .5), 0, .5, 1, 10 )
			);
			t.moveForward(
				p.map(noise, 0, 1, 2, 9)
			);
			if(t.y < 0){
				reset();
				break;
			}
		}
	}
}

let myp5 = new p5(sketch)