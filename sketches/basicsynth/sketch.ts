import p5 from "p5"
import * as Tone from "tone"
import {placeButton, map} from "../../lib/general"

var synth : Tone.FMSynth;
var fft : Tone.FFT;
placeButton("Set Up", function(){
	synth = new Tone.FMSynth();
	synth.toDestination();

	fft = new Tone.FFT();
	synth.connect(fft);

	new p5(sketch);
});

var sketch = function(p : p5){
	p.setup = function(){
		p.createCanvas(400, 300);
		p.strokeWeight(2);
	}
	var playing = false;
	p.draw = function(){
		p.background(105, 97, 89);

		if(playing){
			p.stroke(219,118,18);
			var freq = fft.getValue();
			for(let c = 0; c < freq.length - 1; c++){
				p.line(
					map(c, 0, freq.length, 0, p.width),
					map(freq[c], -500, 0, p.height, 0),
					map(c+1, 0, freq.length, 0, p.width),
					map(freq[c+1], -500, 0, p.height, 0),
				)
			}
		}

		p.stroke(150);
		p.line(p.width/2,0, p.width/2, p.height);
		p.line(0, p.height/2, p.width, p.height/2);
	}
	p.mousePressed = function(){
		synth.triggerAttack(pitch());
		synth.modulationIndex.linearRampTo(modulation(), .2)
		playing = true;
	}
	p.mouseDragged = function(){
		synth.frequency.linearRampTo(pitch(), .2)
		synth.modulationIndex.linearRampTo(modulation(), .2)
		
	}
	p.mouseReleased = function(){
		synth.triggerRelease();
		playing = false;
	}

	function pitch(){
		return p.map(p.mouseX, 0, p.width, 80, 680);
	}
	function modulation(){
		return p.map(p.mouseY, 0, p.height, 0, 30)
	}
}