import { Segment, Rectangle, Color} from 'paper';
import {setup, v} from "../../lib/paper"
import {random, map1} from "../../lib/general"
import {noise} from "@chriscourses/perlin-noise"

let color1 = "#ADE0D5"
let color2 = "#2E4C8C"

let p = setup([1920/4, 1080/4]);

let paths : paper.Path[] = []
let times : number[] = []

let background = new p.Path.Rectangle(new Rectangle(0, 0, p.view.size.width, p.view.size.height))

for(let y = 0; y < p.view.size.height; y += 10){
	let path = new p.Path(
		[
			new Segment(v(-200, y), v(0, 0), v(550, 0)),
			new Segment(v(p.view.size.width + 200, y), v(-550, 0), v(0, 0)),
			new Segment(v(p.view.size.width + 200, p.view.size.height)),
			new Segment(v(-200, p.view.size.height))
		]
	)
	paths.push(path)
	times.push(0)
}

let compound = new p.CompoundPath({
	children: paths,
	fillRule : "evenodd"
})

let gradient1 = {
		stops: [
			[color1, 0],
			[color2, .25],
			[color1, .5],
			[color2, .75],
			[color1, 1],
		],
		radial : false
	}
let gradient2 = {
		stops: [
			[color1, 0],
			[color2, .25],
			[color1, .5],
			[color2, .75],
			[color1, 1],
		],
		radial : false
	}

compound.fillColor = new Color({
	gradient: gradient1,
	origin: v(0, -100),
	destination: v(0, p.view.size.height + 100)
})

background.fillColor = new Color({
	gradient: gradient2,
	origin: v(0, -100),
	destination: v(0, p.view.size.height + 100)
})

const avgtimescale = .7
const lrdelay = .6
const vec = 100
const gradienttimescale = .1;

p.view.onFrame = function(event){
	for(let c = 0; c < paths.length; c++){
		//this shakes up the speeds: differs per line and over time
		times[c] += event.delta * avgtimescale * map1(noise(event.time * .2, c * 20), .8, 1.2)

		paths[c].segments[0].handleOut.y = map1(noise(times[c], c * 10), -vec, vec)
		paths[c].segments[1].handleIn.y = map1(noise(times[c] + lrdelay, c * 10), -vec, vec)
	};

	let bkvals = [noise(event.time * gradienttimescale, 10), noise(event.time * gradienttimescale, 20), noise(event.time * gradienttimescale, 30)].sort();
	let cvals = [noise(event.time * gradienttimescale, 50), noise(event.time * gradienttimescale, 60), noise(event.time * gradienttimescale, 70)].sort();

	background.fillColor.gradient.stops[1].offset = bkvals[0];
	background.fillColor.gradient.stops[2].offset = bkvals[1];
	background.fillColor.gradient.stops[3].offset = bkvals[2];
	compound.fillColor.gradient.stops[1].offset = cvals[0];
	compound.fillColor.gradient.stops[2].offset = cvals[1];
	compound.fillColor.gradient.stops[3].offset = cvals[2];
}