import p5 from "p5";
import Turtle from "../../lib/turtle"
var sketch = function(p : p5){
	let t : Turtle;
	p.setup = function(){
		p.createCanvas(500, 500);
		p.noFill();
		p.stroke(255);
		p.background(50);
		p.noLoop();
		t = new Turtle(p);
	}
	p.draw = function(){
		t.penUp();
		t.moveTo(330, 340);
		t.turnTo(-70);
		t.penDown();
		drawBranch(130, 60);
	}
	function drawBranch(length : number, n : number){
		if (length < 2 || n < 0) {
			return;
		}
	
		// draw this branch
		p.strokeWeight(length / 30);
		t.moveForward(length)

		// left child
		t.pushState();
		t.turnRight(34);
		drawBranch(length * 0.43, n-1);
	
		t.popState();
	
		// right child
		t.pushState();
		t.turnLeft(66);
		drawBranch(length, n-3);
		t.popState();
	
	}
}