import {processArray, gen} from "../../lib/markov"
import {placeButton} from "../../lib/general"
import * as Tone from "tone"

var marseillaise = "G4 E4 G4 C5 C5 D5 D5 G5 E5 C5 E5 C5 A5 F5 D5 B5 C5 C5 D5 E5 E5 E5 F5 E5 E5 B5 B5 E5 F5 F5 F5 G5 F5 G5 G5 G5 C5 G5 E5 C5 G4 G4 G4 G4 B5 D5 F5 D5 D5 C5 C5 A5 G4 C5 C5 C5 B5 C5 D5 D5 E5 E5 E5 E5 F5 G5 D5 D5 C5 C5 E5 D5 C5 C5 B5 G5 G5 E5 C5 C5 D5 G5 G5 E5 C5 C5 D5 G4 C5 C5 E5 F5 G5 A6 B5 A6 G5 E5 F5 D5 C5".trim().split(" ")
var model = processArray(marseillaise, 8)
var generator = gen(model)

var synth : Tone.FMSynth;

function playanote(){
	var result = generator.next();

	if(!result.done && !!result.value){
		synth.triggerAttackRelease(result.value, .4);
	}else{
		Tone.Transport.stop();
	}
}

var initted = false;

placeButton("Start", function(){
	if(!initted){
		synth = new Tone.FMSynth();
		synth.toDestination();
		Tone.Transport.scheduleRepeat(playanote, .5);
		placeButton("Stop", function(){Tone.Transport.pause()})
		drawflag();
		initted = true;
	}

	Tone.Transport.start();
})

function drawflag(){
	var img = document.createElement("img");
	img.setAttribute("src", "/assets/markovmusic/france.gif");
	document.body.appendChild(document.createElement("br"))
	document.body.appendChild(img);
}

console.log(model)