import {setup, v} from "../../lib/paper"
import { Segment, Color } from "paper"
import {noise} from "@chriscourses/perlin-noise"
import { map1, map } from "../../lib/general";

let p = setup([800, 800]);
const cellw = 35;
const cellh = 25;
const link = 6;
const smoothcoef = .2
const noisef = .01
const mincoef = .6
/**
 * 
 * @param center the center of the chevron, or rather its apex
 * @param coef how far the arms go
 * @param orient true for pointing up, false for pointing down
 */
function drawChevron(center : paper.Point, coef : number, orient : boolean){
	const left = v(-cellw/2 + link/2, (orient ? cellh : -cellh) * coef)
	const right = v(cellw/2 - link/2, (orient ? cellh : -cellh) * coef)
	let line = new p.Path([
		new Segment(center.add(left)),
		new Segment(center, v(-smoothcoef*cellw, 0), v(smoothcoef*cellw, 0)),
		new Segment(center.add(right))
	])
	line.closed = false;
	line.strokeColor = new Color(0, 0, 255, map(coef, mincoef , 1, 0, 1))
}
for(let y = 0; y < p.view.size.height; y += cellh * 1.2){
	for(let x = 0; x < p.view.size.width; x += cellw){
		drawChevron(v(x, y), map1(noise(x * noisef, y * noisef), mincoef, 1), false)
	}
	for(let x = 0; x < p.view.size.width; x += cellw){
		let xx = x + cellw/2
		let yy = y - cellh * .17
		drawChevron(v(xx, yy), map1(noise(xx * noisef, yy * noisef), mincoef, 1), true)
	}	
}