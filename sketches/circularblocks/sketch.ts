import p5 from "p5";
import {genarray} from "../../lib/general"
type State = 0 | 1 | 2
interface PRadioButton extends p5.Element{
	option: (value : any, label? : any) => void
}

var sketch = function(p : p5){
	const xbl = 20;
	const ybl = 20;
	const xc = 20;
	const yc = 20;
	const perDraw = 500;

	const redcolor = p.color(255, 0, 0);
	const blackcolor = p.color(0);

	let grid : State[][];
	let rule : PRadioButton[];
	let drawer : Generator<() => void, void, unknown>;
	let lastDraw : number;

	function resetDraw(){
		p.background(255);
		drawer = redrawSpiralSteps();
		grid = genarray(xc, (_) => (genarray(yc, (_) => 0)))
	}
	p.setup = function(){
		p.createCanvas(xbl*xc, ybl*yc);

		rule = genarray(5, (c) => {
			p.createP(c+" colored blocks:")
			let radio = p.createRadio("" + c) as unknown as PRadioButton;
			radio.option(0, "None");
			radio.option(1, "Red");
			radio.option(2, "Black");
			return radio;
		});
		let redrawButton = p.createButton("Redraw");
		redrawButton.mouseClicked(resetDraw)

		p.noStroke();

		resetDraw();
		p.frameRate(40);
	}

	p.draw = function(){
		if(/*p.millis() - lastDraw> perDraw*/true){
			let draw = drawer.next();
			if(!draw.done && draw.value)
				draw.value();

			lastDraw = p.millis();
		}
	}
	function* redrawSpiralSteps(){
		let x = xc / 2 - 1;
		let y = yc / 2 - 1;
		yield () => {drawBlock(2, x, y)};

		let linelen = 1;
		while(true){
			for(let c = 0; c < linelen; c++){
				if(++x >= xc) return;
				yield drawBlockRuleThunk(x, y);
			}
			for(let c = 0; c < linelen; c++){
				if(++y >= yc) return;
				yield drawBlockRuleThunk(x, y);
			}
			linelen++;
			for(let c = 0; c < linelen; c++){
				if(--x < 0) return;
				yield drawBlockRuleThunk(x, y);
			}
			for(let c = 0; c < linelen; c++){
				if(--y < 0) return;
				yield drawBlockRuleThunk(x, y);
			}
			linelen++;
		}
	}
	function drawBlockRuleThunk(x : number, y : number){
		return () => drawBlockRule(x, y)
	}
	function drawBlockRule(x : number, y : number){
		let count = countCircumference(x, y)
		if (count >= rule.length)
			count = rule.length-1;
		let color = rule[count].value() as number as State;
		drawBlock(color, x, y);
	}
	function drawBlock(color : State, x : number, y : number) : void{
		grid[x][y] = color;
		if(color != 0){
			if(color == 1)
				p.fill(redcolor);
			else if(color == 2)
				p.fill(blackcolor);
			p.rect(xbl*x, ybl*y, xbl, ybl);
		}
	}

	function isFull(x : number, y : number) : boolean{
		if(x < 0 || x >= xc || y < 0 || y >= yc) return false;
		return grid[x][y] != 0 || grid[x][y] === undefined;
	}

	const circumferenceOffsets = [
		[-1,0],
		[1,0],
		[0,-1],
		[0, 1],
		[-1,-1],
		[-1,1],
		[1,1],
		[1,-1]
	]
	function countCircumference(x : number, y : number){
		let sum = 0;
		for(let [xoff, yoff] of circumferenceOffsets){
			if(isFull(xoff+x, yoff+y)){
				sum++;
			}
		}
		return sum;
	}
}

let myp5 = new p5(sketch)