import p5 from "p5";
import {dither, DitherImage, DitherMatrix} from "../../lib/dither";
import {genarray} from "../../lib/general";
var sketch = function(p : p5){
	let img : p5.Image;
	let dithermatrix : DitherMatrix;
	let ditherimg : DitherImage[];
	let ditherout : boolean[][][];
	let textbox = document.createElement("textarea");
	let iterationcount = 0;

	p.preload = function(){
		img = p.loadImage("/assets/dither/img.png");
	}
	p.setup = function(){
		p.createCanvas(500, 500);

		p.frameRate(1.2);
		p.pixelDensity(1);
		document.body.append(textbox);
		img.loadPixels();
		ditherimg = [
			{
				gen : (x, y) => (img.pixels[ (x + y * img.width) * 4 ] / 255),
				w : img.width,
				h : img.height
			},
			{
				gen : (x, y) => (img.pixels[ (x + y * img.width) * 4 + 1] / 255),
				w : img.width,
				h : img.height
			},
			{
				gen : (x, y) => (img.pixels[ (x + y * img.width) * 4 + 2] / 255),
				w : img.width,
				h : img.height
			}
		]
		ditherout = [
			genarray(img.width, () => (new Array<boolean>(img.height))),
			genarray(img.width, () => (new Array<boolean>(img.height))),
			genarray(img.width, () => (new Array<boolean>(img.height)))
		]
	}
	p.draw = function(){
		p.background(0)
		redrawRandom();
	}
	function redrawInput(){
		dithermatrix = parseMatrixString(textbox.value)
		update();
		drawImage();
	}
	function redrawRandom(){
		dithermatrix = randomMatrixString();
		textbox.value = 
			dithermatrix.matrix.slice(0, 7).reduce((a : string, b : number) => (a + " " + b.toString().substr(0, 4)), "")
			+ "\n" + 
			dithermatrix.matrix.slice(7, 14).reduce((a : string, b : number) => (a + " " + b.toString().substr(0, 4)), "")
			+ "\n" + 
			dithermatrix.matrix.slice(14, 21).reduce((a : string, b : number) => (a + " " + b.toString().substr(0, 4)), "")
			+ "\n" + 
		update();
		drawImage();
	}
	function update(){
		dither(ditherimg[0], ditherout[0], dithermatrix);
		dither(ditherimg[1], ditherout[1], dithermatrix);
		dither(ditherimg[2], ditherout[2], dithermatrix);
	}
	function drawImage(){
		p.loadPixels();
		for(let x = 0; x < ditherout[0].length; x++){
			for(let y = 0; y < ditherout[0][x].length; y++){
				p.pixels[(x + y * p.width) * 4] = ditherout[0][x][y] ? 255 : 0
				p.pixels[(x + y * p.width) * 4 + 1] = ditherout[1][x][y] ? 255 : 0
				p.pixels[(x + y * p.width) * 4 + 2] = ditherout[2][x][y] ? 255 : 0
				p.pixels[(x + y * p.width) * 4 + 3] = 255
			}
			
		}
		p.updatePixels();
	}
	function parseMatrixString (str : string) : DitherMatrix{
		let out : number[] = [];
		let total = 0;
		for(let c of str.split(/[ \n]+/)){
			let num = parseInt(c)

			if(!isNaN(num)){
				out.push(num);
				total += num;
			}
		}
		out.length = 7 * 3;
		return {
			minx : -3,
			maxx : 3,
			coefficient : 1 / total,
			matrix : out
		}
	}
	function randomMatrixString(){
		let out : number[] = []
		for(let c = 0; c < 7 * 3; c++)
			out.push(p.map(p.noise(iterationcount / 7 + c * 2), 0, 1, 0, 2.3))
		iterationcount ++;
		return{
			minx : -3,
			maxx : 3,
			coefficient : 1,
			matrix : out
		}

	}
}

let myp5 = new p5(sketch)