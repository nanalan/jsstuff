import p5 from "p5"
import * as Tone from "tone"
import {placeButton, map} from "../../lib/general"

var synth : Tone.PolySynth;

const widthnum = 6;
const deltathreshold = 20;

function extractTones(image : p5.Image) : [number, number][][]{
	let tones : [number, number][][] = [];
	image.loadPixels();
	for(let s = 0; s < image.width; s += widthnum){
		
		//collate strip to single array
		let strip = new Array<number>(image.height).fill(0);
		for(let y = 0; y < image.height; y++){
			for(let x = s; x < s + widthnum; x++){
				let [r, g, b, _] = image.get(x, y);
				strip[y] += r + g + b
			}
		}
		
		//get maxima
		let tonestrip : [number, number][] = [];
		let lastmax : number = strip[0];
		let lastsigchange : number = strip[0];
		let mode : boolean = false;
		for(let c = 1; c < strip.length; c++){
			let diff = strip[c] - lastsigchange;
			
			if(diff > deltathreshold){
				lastsigchange = strip[c];
				if(mode == false){
					tonestrip.push([c, Math.abs(lastmax - strip[c])]);
					lastmax = strip[c];
					mode = true;
				}
			}else if(diff < -deltathreshold){
				lastsigchange = strip[c];
				if(mode == true){
					tonestrip.push([c, Math.abs(lastmax - strip[c])]);
					lastmax = strip[c];
					mode = false;
				}
			}
		}
		tones.push(tonestrip)
		console.log(tonestrip)
	}
	return tones;
}

var sketch = function(p : p5){
	let img : p5.Image;
	let tones : [number, number][][];
	p.preload = function(){
		img = p.loadImage("/assets/astroson/hubble.png");

	}
	p.setup = function(){
		p.createCanvas(600, 600);
		p.pixelDensity(1);
		p.frameRate(2);
		p.fill(255,162,23);
		p.noStroke();

		tones = extractTones(img);
		console.log(tones)
	}
	let x = 0;
	p.draw = function(){
		//draw it
		p.image(img, 0, 0);
		for(let y of tones[x]){
			p.circle(
				map(x, 0, tones.length, 0, img.width),
				y[0],
				5
			)
		}

		//play it
		for(let tone of tones[x]){
			synth.triggerAttackRelease(
				map(tone[0], 0, img.height, 100, 800),
				.5,
				undefined,
				tone[1] / 768
			)
		}

		x++;
		if(x >= tones.length)
			x = 0;
	}
}

placeButton("Set Up", function(){
	synth = new Tone.PolySynth({
		maxPolyphony:100
	});
	synth.toDestination();

	new p5(sketch);
})