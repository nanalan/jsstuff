import p5, { Color } from "p5";
type xy = [number, number];
var sketch = function(p : p5){
	const angPerMs = .06;
	let a1 : p5.Element, f1 : p5.Element, a2 : p5.Element, f2 : p5.Element, a3 : p5.Element, f3 : p5.Element, a4 : p5.Element, f4 : p5.Element;
	let lastPoint : xy;
	let lastAngle : number;
	function getPoint(angle : number) : xy{
		let x = 0;
		let y = 0;
		x += a1.value() as number * p.sin(f1.value() as number * angle)
		y += a1.value() as number * p.cos(f1.value() as number * angle)
		x += a2.value() as number * p.sin(f2.value() as number * angle)
		y += a2.value() as number * p.cos(f2.value() as number * angle)
		x += a3.value() as number * p.sin(f3.value() as number * angle)
		y += a3.value() as number * p.cos(f3.value() as number * angle)
		x += a4.value() as number * p.sin(f4.value() as number * angle)
		y += a4.value() as number * p.cos(f4.value() as number * angle)
		return [x, y];
	}
	p.setup = function(){
		p.createCanvas(300, 300)
		p.angleMode(p.DEGREES)
		p.colorMode(p.HSL)

		p.createSpan("Amplitude:")
		a1 = p.createSlider(0, 50, 50);
		p.createSpan("Frequency:")
		f1 = p.createSlider(0, 20, 1.6);
		p.createP("");
		p.createSpan("Amplitude:")
		a2 = p.createSlider(0, 50, 30);
		p.createSpan("Frequency:")
		f2 = p.createSlider(0, 20, 3);
		p.createP("");
		p.createSpan("Amplitude:")
		a3 = p.createSlider(0, 50, 0);
		p.createSpan("Frequency:")
		f3 = p.createSlider(0, 20, 9);
		p.createP("");
		p.createSpan("Amplitude:")
		a4 = p.createSlider(0, 50, 0);
		p.createSpan("Frequency:")
		f4 = p.createSlider(0, 20, 17);
		p.createP("");

		let button = p.createButton("Reset")
		button.mouseClicked(function(){p.background(255)})

		lastAngle = 0;
		lastPoint = getPoint(0);
	}

	p.draw = function(){
		
		lastAngle += p.deltaTime * angPerMs;
		p.stroke(lastAngle % 360, 100, 78)
		let point = getPoint(lastAngle)
		p.line(p.width / 2 + point[0], p.height / 2 + point[1], p.width / 2 + lastPoint[0], p.height / 2 + lastPoint[1])
		lastPoint = point
	}
}

let myp5 = new p5(sketch)
