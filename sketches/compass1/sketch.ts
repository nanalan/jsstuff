import * as Tone from "tone"
import {} from "paper/dist/paper-core"
import {setup, v} from "../../lib/paper"
import { map } from "../../lib/general";



let button = document.createElement("button");
button.textContent = "Set Up";
button.addEventListener("click", useractivated);
document.body.appendChild(button);
function useractivated(){
	let p = setup([window.innerWidth, window.innerHeight]);

	const center = v(p.view.size.width/2, p.view.size.height/2)

	//set up graphics
	var errortext = new p.PointText({
		point: new p.Point(p.view.size.width/2, p.view.size.height/2),
		justification: "center",
		fillColor: "black"
	});
	
	var bkgd = new p.Path.Circle({
		center : v(p.view.size.width/2, p.view.size.height/2),
		radius : Math.sqrt(Math.pow(p.view.size.width, 2) + Math.pow(p.view.size.height, 2)) / 2,
		fillColor: {
			gradient:{
				stops: ["yellow", "red"]
			},
			origin: v(0, 0),
			destination: v(p.view.size.width, p.view.size.height)
		},
		applyMatrix: false
	})
	
	var anglelegend = new p.PointText({
		content: "",
		fillColor: "white",
		justification:"center",
		fontSize:"100px",
		point:v(p.view.size.width / 2, p.view.size.height - 100)
	});

	//set up audio
	var audiocontext = new AudioContext();
	Tone.setContext(audiocontext);
	var osc1 = new Tone.AMOscillator();
	var osc2 = new Tone.AMOscillator();
	var panner = audiocontext.createPanner();
	panner.coneInnerAngle = 20;
	panner.coneOuterAngle = 180;
	panner.coneOuterGain = .05
	panner.positionZ.setValueAtTime(40, audiocontext.currentTime + .05)

	osc1.connect(panner);
	osc2.connect(panner);
	panner.connect(audiocontext.destination);

	Tone.Transport.start();
	osc1.start();
	osc2.start();
	
	//set up reaction
	var last = 0;
	function updateAngle(angle : number){
		anglelegend.content = ("" + (180 - angle)).substring(0, 5) + " degrees";
		
		bkgd.matrix.reset();
		bkgd.matrix.rotate((180 + angle), center);

		const momentlater = audiocontext.currentTime + .05;

		var rad = angle * Math.PI / 180;
		panner.orientationX.setValueAtTime(Math.sin(rad), momentlater);
		panner.orientationZ.setValueAtTime(Math.cos(rad), momentlater);

		if(window.performance.now() - last > 100){
			osc1.frequency.setValueAtTime(
				map(Math.abs(180 - angle), 180, 0, 440, 330),
				momentlater
			)
	
			osc2.frequency.setValueAtTime(
				map(Math.abs(180 - angle), 180, 0, 550, 120),
				momentlater
			)
			last = window.performance.now();
		}
	}
	window.addEventListener("deviceorientation", (event) => {updateAngle(event.alpha)});

}