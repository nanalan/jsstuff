import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import fs from "fs"
import path from "path"

let inputs = {};

for(let name of fs.readdirSync( path.resolve(__dirname, "out-ts/sketches") ) ){
	inputs[name] = "out-ts/sketches/" + name + "/sketch.js"
}

export default {
	input: inputs,
	output: {
		dir: "out-rollup/",
		entryFileNames: "[name].js",
		manualChunks:{
			p5:["p5"],
			paper:["paper"],
			tone:["tone"]
		}
	},
	plugins: [
		resolve({
			browser:true,
		}),
		commonjs()
	],
}