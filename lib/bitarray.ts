class BitArray {
	buffer : Uint8Array;
	view : DataView;
	constructor(length : number){
		this.buffer = new Uint8Array(Math.floor(length / 8) + 1);
	}
	get(index : number){
		let bit = index % 8;
		let byte = ( index - bit ) / 8
		return (this.buffer[byte] & (1 << bit)) > 0
	}
	set(index : number, value : boolean){
		let bit = index % 8;
		let byte = ( index - bit ) / 8
		if(value){
			this.buffer[byte] = this.buffer[byte] | (1 << bit)
		}else{
			this.buffer[byte] = this.buffer[byte] & (~(1 << bit))
		}
	}
}
class BitArray2D{
	x : number;
	array : BitArray;
	constructor(x : number, y : number){
		this.array = new BitArray(x * y);
		this.x = x;
	}
	get(x : number, y : number){
		return this.array.get(y * this.x + x);
	}
	set(x : number, y : number, value : boolean){
		return this.array.set(y * this.x + x, value);
	}
}
