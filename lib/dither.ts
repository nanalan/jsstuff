import {genarray} from "./general"
var floydsdither = {
	minx : -1,
	maxx : 1,
	coefficient : 1/20,
	matrix : [0, 0, 7, 3, 5, 1]
}

//so we don't have to reallocate rows each time
var _ditherStatic : {
	row0 : Float32Array,
	row1 : Float32Array,
	row2 : Float32Array
} | null = null;
function _getDitherStatic(width : number){
	if(!_ditherStatic){
		_ditherStatic = {
			row0 : new Float32Array(width),
			row1 : new Float32Array(width),
			row2 : new Float32Array(width)
		}
	}
	if(_ditherStatic.row0.length < width){
		_ditherStatic = {
			row0 : new Float32Array(width),
			row1 : new Float32Array(width),
			row2 : new Float32Array(width)
		}
	}
	_ditherStatic.row0.fill(0);
	_ditherStatic.row1.fill(0);
	_ditherStatic.row2.fill(0);
	return _ditherStatic;
}
export type DitherMatrix = {
	minx : number,
	maxx : number,
	coefficient : number,
	matrix : number[]
}
export type DitherImage = {
	gen : (x : number, y : number) => number, 
	w : number, 
	h : number
}
export function dither(image : DitherImage, writeTo? : boolean[][], dithermatrix? : DitherMatrix){
	let {row0, row1, row2} = _getDitherStatic(image.w);

	if(!writeTo)
		writeTo = genarray(image.w, () => (new Array<boolean>(image.h)))
	
	if(!dithermatrix)
		dithermatrix = floydsdither;
	
	for(let y = 0; y < image.h; y++){
		for(let x = 0; x < image.w; x++){
			let level = image.gen(x, y) + row0[x];
			if(level > .5){
				writeTo[x][y] = true;
				level -= 1;
			}else{
				writeTo[x][y]= false;
			}
			dodither(level, x, dithermatrix, row0, row1, row2);
		}

		let row0_old = row0;
		row0 = row1;
		row1 = row2;
		row2 = row0_old;
		row2.fill(0);
	}

	return writeTo;
}
function dodither(level : number, x : number, dithermatrix : DitherMatrix, row0 : Float32Array, row1 : Float32Array, row2 : Float32Array){
	let mati = 0;
	let {minx, maxx, matrix, coefficient} = dithermatrix;
	for(let xx = x + minx; xx <= x + maxx; xx++){
		if(x < 0 || x >= row0.length)
			continue;
		if(mati >= matrix.length)
			return;

		row0[xx] = level * matrix[mati] * coefficient;
		mati++;
	}
	for(let xx = x + minx; xx <= x + maxx; xx++){
		if(x < 0 || x >= row1.length)
			continue;
		if(mati >= matrix.length)
			return;
			
		row1[xx] = level * matrix[mati] * coefficient;
		mati++;
	}
	for(let xx = x + minx; xx <= x + maxx; xx++){
		if(x < 0 || x >= row2.length)
			continue;
		if(mati >= matrix.length)
			return;
			
		row2[xx] = level * matrix[mati] * coefficient;
		mati++;
	}
	
}