type intern = number
type internstring = string

export type MarkovModel<T extends string | number> = {
	interns : T[];
	matches : Map<internstring, intern[]>;
	depth : number;
	start : intern[];
}

function toInternstring(interns : intern[]){
	return interns.reduce<string>((acc, val) => (acc + String.fromCharCode(val)), "")
}

function intern<A extends string | number>(seq : A[]) : {seq : intern[], interns : A[]}{
	var interns : A[] = []

	var internedseq : intern[] = seq.map(
		(item) => {
			let i = interns.findIndex((a) => a === item)
			if(i === -1){
				interns.push(item);
				i = interns.length - 1;
			}
			return i
		}
	)

	return {seq : internedseq, interns : interns}
}

function processMarkov(seq : intern[], depth = 1) : Map<internstring, intern[]>{
	var matches : Map<internstring, intern[]> = new Map();

	for(let c = 0; c < seq.length; c++){
		for(let matchlen = 1; matchlen <= depth && matchlen <= c; matchlen++){
			let matchstr = toInternstring(seq.slice(c - matchlen, c))

			if(matches.has(matchstr)){
				matches.get(matchstr).push(seq[c])
			}else{
				matches.set(matchstr, [seq[c]])
			}
		}
	}

	return matches
}

export function* gen<T extends string | number>({interns, matches, depth, start} : MarkovModel<T>){
	var history = start.slice(); //to copy it

	while(true){
		let possibilities : intern[] | undefined = undefined;

		let adjusteddepth = Math.ceil(Math.max(Math.random(), Math.random(), Math.random()) * depth);

		for(let matchlen = Math.min(adjusteddepth, history.length); matchlen > 0 && possibilities === undefined; matchlen--){
			possibilities = matches.get(toInternstring(history.slice(history.length - matchlen)))
		}

		if(possibilities === undefined)
			return;

		let next = 
			possibilities[
				Math.floor(
					Math.random() * possibilities.length
				)
			]

		yield interns[next];

		history.push(next)

		if(history.length > depth + 50)
			history = history.slice(history.length - depth)
	}
}

export function processArray<A extends string | number>(nums : A[], depth : number) : MarkovModel<A>{
	let {seq, interns} = intern(nums)
	let matches = processMarkov(seq, depth)
	let start = nums.slice(0, depth).map((obj) => interns.indexOf(obj))
	return {
		interns:interns,
		matches:matches,
		depth:depth,
		start:start
	}
}