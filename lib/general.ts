export function genarray<T>(len : number, func : (number : number) => T) : Array<T>{
	let arr = new Array<T>(len);
	for(let c = 0; c < arr.length; c++)
		arr[c] = func(c);
	return arr;
}
export function random(a : number, b? : number){
	if(b === undefined)
		return Math.random() * a
	else
		return Math.random() * (b - a) + a
}
/**
 * Maps v from a range to another range, linearly.
 * @param v the input number
 * @param min1 the minimum input
 * @param max1 the maximum input
 */
export function map(v, min1, max1, min2, max2){
	return (v - min1) * (max2 - min2) / (max1 - min1) + min2
}
/**
 * Maps v, assuming an input range of (0, 1)
 * @param v the input number
 * @param min2 the minimum of the output range
 * @param max2 the maximum of the output range
 */
export function map1(v, min2, max2){
	return v * (max2 - min2) + min2;
}

export function placeButton(message : string, callback : (...a : any) => any):void{
	var button = document.createElement("button");
	button.innerText = message;
	button.addEventListener("click", callback);
	document.body.appendChild(button);
}

/**
 * Finds v's modulus of m. If negative, don't apply absolute value.
 */
export function posmod(v : number, m : number){
	var mod = v % m;
	if(mod < 0)
		return mod + m
	return mod
}

export function limitmin(min : number) : (number) => number{
	return (num : number) => num >= min ? num : min - 1;
}
export function limitmax(max : number) : (number) => number{
	return (num : number) => num < max ? num : max - 1;
}