import { PaperScope, Project, Point} from "paper"
export function setup(dims? : [number, number]){
	var style = document.createElement("link")
	style.setAttribute("rel", "stylesheet")
	style.setAttribute("href", "/assets/paper/paper.css")
	document.body.appendChild(style)

	var canvas = document.createElement("canvas")
	document.body.appendChild(canvas)
	if(dims){
		canvas.setAttribute("width", "" + dims[0])
		canvas.setAttribute("height", "" + dims[1])
	}
	
	let p = new PaperScope();
	let project = new Project(canvas);

	return p
}

export function v(x : number, y : number){
	return new Point(x, y)
}